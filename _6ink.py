#!/usr/bin/env python3
########################################################################
#                                                                      #
#           66666666     iiii                    kkkkkkkk              #
#          6::::::6     i::::i                   k::::::k              #
#         6::::::6       iiii                    k::::::k              #
#        6::::::6                                k::::::k              #
#       6::::::6       iiiiiii nnnn  nnnnnnnn     k:::::k    kkkkkkk   #
#      6::::::6        i:::::i n:::nn::::::::nn   k:::::k   k:::::k    #
#     6::::::6          i::::i n::::::::::::::nn  k:::::k  k:::::k     #
#    6::::::::66666     i::::i nn:::::::::::::::n k:::::k k:::::k      #
#   6::::::::::::::66   i::::i   n:::::nnnn:::::n k::::::k:::::k       #
#   6::::::66666:::::6  i::::i   n::::n    n::::n k:::::::::::k        #
#   6:::::6     6:::::6 i::::i   n::::n    n::::n k:::::::::::k        #
#   6:::::6     6:::::6 i::::i   n::::n    n::::n k::::::k:::::k       #
#   6::::::66666::::::6i::::::i  n::::n    n::::nk::::::k k:::::k      #
#    66:::::::::::::66 i::::::i  n::::n    n::::nk::::::k  k:::::k     #
#      66:::::::::66   i::::::i  n::::n    n::::nk::::::k   k:::::k    #
#        666666666     iiiiiiii  nnnnnn    nnnnnnkkkkkkkk    kkkkkkk   #
#                                                                      #
#                         6ink is not klipper                          #
########################################################################

import gi
import os
import logging
import configparser
gi.require_version('Gtk', '3.0')
gi.require_version('Gdk', '3.0')
from gi.repository import Gtk, Gdk
from pystray       import Icon as picon, Menu as m, MenuItem as item
from PIL           import Image

# Global variables

logging.basicConfig(level=logging.INFO)
logger  = logging.getLogger(__name__)
i       = None
mod     = 1
title   = '6ink'
clip    = None
state   = {}
notifs  = False
radiocb = False
neot    = False
appdir  = os.getenv('APPDIR') 
confile = os.getenv("HOME")   + '/.config/6ink/6ink.conf'

if (type(appdir) is str):                   # we are inside the appimage
    b_png   = appdir + '/share/img/black.png'
    w_png   = appdir + '/share/img/white.png'
else:                                       # we are in "normal" mode
    b_png   = 'img/black.png'
    w_png   = 'img/white.png'

# Functions

'''
    Write configuration File
'''
def writeConfig():
    config = configparser.ConfigParser()
    config['Settings'] = {
        'DisplayNotifs'  : 'True',
        'RadioCheckbox'  : 'True',
        'NewEntriesOnTop': 'True'
    }
    with open(confile, 'w') as configfile:
        config.write(configfile)

'''
    Read configuration File
'''
def readConfig():
    global radiocb
    global notifs
    global neot

    Config = configparser.ConfigParser()
    Config.read(confile)
    notifs  = Config.getboolean('Settings', 'DisplayNotifs')
    radiocb = Config.getboolean('Settings', 'RadioCheckbox')
    neot    = Config.getboolean('Settings', 'NewEntriesOnTop')
    logger.info("RadioCheckbox:"   + str(radiocb))
    logger.info("DisplayNotifs:"   + str(notifs))
    logger.info("NewEntriesOnTop:" + str(neot))

'''
    Called when user clicks one entry in the list of saved values
    Activate the checkbox of the selected clipboard value
'''
def check_item(icon, ck):
    global state

    for k in state :
        state[k] = False
    state[ck.text] = not ck.checked
    clip.set_text(ck.text, -1)
    logger.info("Selected cliboard is now {}".format(ck.text))

'''
    Each new clipboard entry will change icon color
'''
def refresh_icon():
    global mod
    global i

    mod = mod + 1
    if ((mod % 2) == 0) :
        i.icon    = Image.open(w_png)
    else :
        i.icon    = Image.open(b_png)
    i.menu=m(
        lambda:(
            item(
                key,
                check_item, 
                checked=lambda ck: state[ck.text],
                radio=radiocb
            ) for key in (reversed(state) if (neot == True) else state)
        )
    )

'''
    Each new clipboard entry will trigger this callback
'''
def callBack(*args):
    new_cb = clip.wait_for_text()
    if type(new_cb) is str :
        for k in state :
            state[k] = False
        if new_cb in state :
            message   = "Clipboard back to :"
        else :
            message   = "Clipboard changed to :"
        state[new_cb] = True
        logger.info(message)
        logger.info(" - " + new_cb)
        logger.info(" st " + str(state))
        refresh_icon()
        if (notifs == True):
            i.notify(title = message, message = new_cb)

'''
    For future : function to reset list
'''
def _clean():
    global state

    state  = {}

'''
    Exit function, for now only available at startup
'''
def _quit():
    exit()

def main():
    global clip
    global i

    # init pstray menu
    i = picon(
        title,
        icon=Image.open(w_png),
        menu=m(
            item(title,   None),
            item("-----", None),
            item("Quit",  _quit)
        )
    )
    # pstray detached mode : 
    i.run_detached()
    # set hook on clipboad event
    clip = Gtk.Clipboard.get(Gdk.SELECTION_CLIPBOARD)
    clip.connect('owner-change', callBack)
    Gtk.main()


if __name__ == '__main__':
    # make sure to run in the executable directory
    # ~ os.chdir(os.path.dirname(os.path.abspath(__file__)))
    print("Running from : " + os.path.dirname(os.path.abspath(__file__)))
    # create .config/6ink folder if not present
    os.makedirs(os.getenv("HOME") + '/.config/6ink/', exist_ok=True)
    # create generic 6ink.conf file if not present
    if (not os.path.isfile(confile)): 
        writeConfig()
    readConfig()
    main()
