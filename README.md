# 6ink

6ink Is Not [Klipper](https://userbase.kde.org/Klipper) !

## Description

6ink, pronouced "sync" /ˈsɪŋk/ is a [KISS](https://en.wikipedia.org/wiki/KISS_principle) clipboard manager for Linux.

It is based on [pystray](https://github.com/moses-palmer/pystray).

It allows you to view your clipboard history and select a previous value.

It runs on GNU/Linux only.

## Test

 On debian :
 
    sudo apt install python3-tk python3-dev
    git clone https://framagit.org/6nonimes/6ink.git
    cd 6ink
    pip install -r requirements.txt
    python3 _6ink.py

## Installation
    TDB

## Used Components
 - [pystray](https://github.com/moses-palmer/pystray)
 - [pillow](https://python-pillow.org/)
 - [cython](https://cython.org/)
 
## Visuals

![image info](img/Screenshot_b.png) ![image info](img/Screenshot_w.png)

## Contributing

Ways to contribute :

 - Suggest a feature
 - Report a bug
 - Fix something and open a merge request
 - Help me document the code
 - Spread the word

## Possible new features

 - [x] Add configuration file to :
    - [x] Disable notifications
    - [x] Select display order
    - [x] Change checkbox mode (square or radio)
 - [ ] Create AppImage
 - [ ] [Setup release](https://framagit.org/help/user/project/releases/index#creating-a-release-by-using-a-cicd-job)
 - [ ] Display menu on keystroke (meta + v).
 - [ ] Handle imgs.
 - [ ] Handle left and right click (need to emancipate from pystray?)
    - [ ] Add second menu to quit/reset/configure. 

## Authors and acknowledgment

6nonimes

## License

TDB

## Project status

pre alpha
