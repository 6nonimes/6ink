NAME = 6ink

all : clean $(NAME)

$(NAME):
	cython --embed _6ink.py
	gcc -Os -I. -I/usr/include/python3.10 -o $(NAME) _$(NAME).c -lpython3.10 -lpthread -lm -lutil -ldl

clean:
	@/bin/rm -f _$(NAME).c
	@/bin/rm -f $(NAME)
	@/bin/rm -rf 6ink.AppDir

appimage: all
	mkdir 6ink.AppDir
	mkdir 6ink.AppDir/usr
	mkdir 6ink.AppDir/usr/bin
	mkdir 6ink.AppDir/share/
	mkdir 6ink.AppDir/share/img
	mkdir 6ink.AppDir/usr/lib
	cp 6ink 6ink.AppDir/usr/bin/
	cp img/black.png 6ink.AppDir
	cp img/black.png 6ink.AppDir/share/img
	cp img/white.png 6ink.AppDir/share/img
	cp AppRun 6ink.AppDir
	cp 6ink.desktop 6ink.AppDir
#	cp python.lib 6ink.AppDir/usr/lib
	appimagetool 6ink.AppDir/
